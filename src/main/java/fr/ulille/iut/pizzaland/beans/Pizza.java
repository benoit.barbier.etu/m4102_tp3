package fr.ulille.iut.pizzaland.beans;

import java.util.ArrayList;
import java.util.List;

import fr.ulille.iut.pizzaland.dto.IngredientDto;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDTO;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

public class Pizza {
	private long id;
	private String name;
	private List<Ingredient> composante;
	
	public Pizza() {
		
	}
	public Pizza(long id, String name) {
		this.id = id;
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Ingredient> getComposante() {
		return composante;
	}

	public void setComposante(List<Ingredient> composante) {
		this.composante = composante;
	}

	public long getId() {
		return id;
	}
	
	public static PizzaDto toDto(Pizza i) {
		PizzaDto dto = new PizzaDto();
		dto.setId(i.getId());
		dto.setName(i.getName());
		dto.setContenus(new ArrayList<IngredientDto>());
		return dto;
	}
	
	public static Pizza fromPizzaCreateDto(PizzaCreateDTO dto) {
		Pizza pizza = new Pizza();
		pizza.setName(dto.getName());

		return pizza;
	}
	public void setId(long id) {
		this.id = id;
	}
	


}
