package fr.ulille.iut.pizzaland.dto;

import java.util.List;

public class PizzaDto {
	private long id;
	private String name;
	private List<IngredientDto> contenus;
	
	public String getContenus() {
		return contenus;
	}
	public void setContenus(List<IngredientDto> contenus) {
		this.contenus = contenus;
	}
	public long getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public void setId(long id) {
		this.id = id;
	}
	public void setName(String name) {
		this.name = name;
	} 
	
	
}
